<?php

namespace Drupal\orange_logic_media_library\Plugin\MediaLibrarySource;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\Entity\File;
use Drupal\media_library_extend\Plugin\MediaLibrarySource\MediaLibrarySourceBase;
use Drupal\media_orange_logic\OrangeLogicAssetPublicLinkManager;
use Drupal\media_orange_logic\OrangeLogicManagerInterface;
use Drupal\media_orange_logic\OrangeLogicResultRendererInterface;
use Drupal\media_orange_logic\Plugin\media\Source\OrangeLogicMediaSource;
use Drupal\orange_logic_media_library\OrangeLogicMediaLibraryManagerInterface;
use Drupal\orange_logic_media_library\OrangeLogicMediaLibraryOptionsHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media library pane to pull media from DAM.
 *
 * @MediaLibrarySource(
 *   id = "orange_logic_media_library_source",
 *   label = @Translation("Orange Logic Image"),
 *   source_types = {
 *     "image"
 *   },
 * )
 */
class OrangeLogicMediaLibrarySource extends MediaLibrarySourceBase {

  /**
   * Entity Field Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Orange Logic Manager Service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicManagerInterface
   */
  protected $orangeLogicManager;

  /**
   * Orange logic result renderer service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicResultRendererInterface
   */
  protected $orangeLogicResultRenderer;

  /**
   * Orange logic asset public link manager.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicAssetPublicLinkManager
   */
  protected $orangeLogicAssetPublicLinkManager;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Orange Logic Media Library Manager (service).
   *
   * @var \Drupal\orange_logic_media_library\OrangeLogicMediaLibraryManagerInterface
   */
  protected $manager;

  /**
   * Orange Logic Media Library Options Helper (service).
   *
   * @var \Drupal\orange_logic_media_library\OrangeLogicMediaLibraryOptionsHelperInterface
   */
  protected $optionsHelper;

  /**
   * File URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * OrangeLogicMediaLibrarySource constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\media_orange_logic\OrangeLogicManagerInterface $orange_logic_manager
   *   The orange logic manager service.
   * @param \Drupal\media_orange_logic\OrangeLogicResultRendererInterface $result_renderer
   *   The orange logic result renderer service.
   * @param \Drupal\media_orange_logic\OrangeLogicAssetPublicLinkManager $link_manager
   *   The orange logic asset public link manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\orange_logic_media_library\OrangeLogicMediaLibraryManagerInterface $orange_logic_media_library_manager
   *   Orange Logic Media Library Manager.
   * @param \Drupal\orange_logic_media_library\OrangeLogicMediaLibraryOptionsHelperInterface $options_helper
   *   Orange Logic Media Library Options Helper.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Token $token, FileSystemInterface $file_system, EntityFieldManagerInterface $entity_field_manager, OrangeLogicManagerInterface $orange_logic_manager, OrangeLogicResultRendererInterface $result_renderer, OrangeLogicAssetPublicLinkManager $link_manager, LanguageManagerInterface $language_manager, OrangeLogicMediaLibraryManagerInterface $orange_logic_media_library_manager, OrangeLogicMediaLibraryOptionsHelperInterface $options_helper, FileUrlGeneratorInterface $fileUrlGenerator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $token, $file_system);
    $this->orangeLogicManager = $orange_logic_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->orangeLogicResultRenderer = $result_renderer;
    $this->orangeLogicAssetPublicLinkManager = $link_manager;
    $this->languageManager = $language_manager;
    $this->manager = $orange_logic_media_library_manager;
    $this->optionsHelper = $options_helper;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('file_system'),
      $container->get('entity_field.manager'),
      $container->get('media_orange_logic.manager'),
      $container->get('media_orange_logic.renderer'),
      $container->get('media_orange_logic.asset_public_link.manager'),
      $container->get('language_manager'),
      $container->get('orange_logic_media_library.manager'),
      $container->get('orange_logic_media_library.options_helper'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['field_mapping'] = [
      '#type' => 'details',
      '#description' => $this->t('Entity media field : API Response field'),
      '#title' => $this->t('Field mapping'),
      '#collapsible' => FALSE,
    ];

    $field_names = $this->optionsHelper->getMappingFieldNames();
    $mapping_options = $this->optionsHelper->getMappingOptions();
    $field_mapping_config = $this->getConfiguration()['field_mapping'];
    foreach ($field_names as $field_name) {
      $form['field_mapping'][$field_name] = [
        '#type' => 'select',
        '#title' => $field_name,
        '#options' => $mapping_options,
        '#default_value' => !empty($field_mapping_config[$field_name]) ? $field_mapping_config[$field_name] : NULL,
      ];
    }

    $langcode_options = $this->optionsHelper->getLangcodeOptions();
    $form['default_langcode'] = [
      '#type' => 'select',
      '#title' => $this->t('Default language'),
      '#options' => $langcode_options,
      '#required' => TRUE,
      '#default_value' => $this->getConfiguration()['default_langcode'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $config_key => $config_value) {
      $this->configuration[$config_key] = $config_value;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getSummary() {
    $items = [];

    foreach ($this->getConfiguration()['field_mapping'] as $key => $value) {
      $items[] = sprintf('%s - %s', $key, !empty($value) ? $value : $this->t('None'));
    }

    if (!empty($this->getConfiguration()['default_langcode'])) {
      $items[] = $this->t('Default langcode: <strong>@langcode</strong>', [
        '@langcode' => $this->getConfiguration()['default_langcode'],
      ]);
    }

    return [
      '#theme' => 'item_list',
      '#title' => $this->t('Field mapping'),
      '#list_type' => 'ul',
      '#empty' => $this->t('- None -'),
      '#items' => $items,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'field_mapping' => [],
      'default_langcode' => NULL,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['advanced_search'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter criteria'),
      '#open' => TRUE,
      '#attributes' => ['class' => ['media-orange-logic-search']],
    ];

    $form['advanced_search']['Text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#size' => 20,
      '#maxlength' => 255,
      '#attributes' => [
        'onkeypress' => 'if(event.keyCode==13){this.closest("form").querySelector("input[type=submit]").dispatchEvent(new Event("mousedown"))}',
      ],
    ];

    $form['advanced_search']['SystemIdentifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('System identifier'),
      '#size' => 20,
      '#maxlength' => 255,
      '#attributes' => [
        'onkeypress' => 'if(event.keyCode==13){this.closest("form").querySelector("input[type=submit]").dispatchEvent(new Event("mousedown"))}',
      ],
    ];

    $form['advanced_search']['MediaType'] = [
      '#type' => 'hidden',
      '#value' => [
        'image' => 'image',
      ],
    ];

    $color_options = $this->optionsHelper->getColorOptions();
    $form['advanced_search']['Color'] = [
      '#type' => 'select',
      '#title' => $this->t('Color'),
      '#options' => $color_options,
      '#default_value' => 'all',
    ];

    $orientation_options = $this->optionsHelper->getOrientationOptions();
    $form['advanced_search']['Orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation'),
      '#options' => $orientation_options,
      '#default_value' => 'all',
    ];

    $form['advanced_search']['pagenumber'] = [
      '#type' => 'number',
      '#title' => $this->t('Page'),
      '#title_display' => 'visually_hidden',
      '#min' => 1,
      '#default_value' => 1,
      '#attributes' => ['class' => ['visually-hidden']],
    ];

    $per_page_options = $this->optionsHelper->getPerPageOptions();
    $form['advanced_search']['countperpage'] = [
      '#type' => 'select',
      '#title' => $this->t('Items per page'),
      '#title_display' => 'visually_hidden',
      '#options' => $per_page_options,
      '#default_value' => '10',
      '#attributes' => ['class' => ['visually-hidden']],
    ];

    $form['advanced_search']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Search'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSearchCallback'],
      ],
    ];

    $form['search_results'] = [
      '#type' => 'details',
      '#title' => $this->t('Search results'),
      '#open' => TRUE,
    ];

    $form['search_results']['results'] = [
      '#markup' => '<div id="search-results-items"></div>',
    ];

    // Fake pager managed by js.
    $form['search_results']['pager'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'media-orange-logic-pager',
        ],
      ],
    ];
    $form['search_results']['pager']['previous_page'] = [
      '#type' => 'html_tag',
      '#tag' => 'button',
      '#value' => $this->t('Previous page'),
      '#attributes' => [
        'class' => [
          'button',
          'button--extrasmall',
          'media-orage-logic-prev-button',
          'visually-hidden',
        ],
      ],
    ];
    $form['search_results']['pager']['current_page'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#size' => 1,
      '#title' => $this->t('Current page'),
      '#title_display' => 'visually_hidden',
      '#default_value' => $form_state->getValue('page_number') ?? 1,
    ];
    $form['search_results']['pager']['next_page'] = [
      '#type' => 'html_tag',
      '#tag' => 'button',
      '#value' => $this->t('Next page'),
      '#attributes' => [
        'class' => [
          'button',
          'button--extrasmall',
          'media-orage-logic-next-button',
        ],
      ],
    ];

    $form['selected_media'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Selected assets from the database'),
      '#collapsible' => TRUE,
    ];

    $form['selected_media']['thumbnails'] = [
      '#markup' => '<div id="selected-media-items">' . $this->t('No chosen assets. Please click on a asset to add it to this section') . '</div>',
    ];

    // Hidden input to store the selected assets.
    $form['selected_media']['selected_assets'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'selected-assets',
      ],
    ];

    // Add the required libraries.
    $form['#attached']['library'][] = 'media_orange_logic/entity_browser_ui';

    $form['actions']['submit']['#value'] = $this->t('Import into media library');

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getResults(array $form, FormStateInterface $form_state) {
    $prepared_entities = $this->prepareEntities($form, $form_state);
    $saved_entities = $this->manager->saveEntities($prepared_entities);

    $results = [];

    /** @var \Drupal\media\Entity\Media $image */
    foreach ($saved_entities as $id => $image) {
      $source_field = $this->getSourceField();
      $file = File::load($image->$source_field->target_id);
      $results[] = [
        'id' => $id,
        'label' => $image->getName(),
        'preview' => [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'src' => $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri()),
            'alt' => $image->getName(),
            'title' => $image->getName(),
          ],
        ],
      ];
    }

    return $results;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityId(string $selected_id) {
    // This method is left empty.
  }

  /**
   * Called when 'Search' pressed.
   *
   * @param array $form
   *   Current search form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current search form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax response object.
   */
  public function ajaxSearchCallback(array &$form, FormStateInterface $form_state) {
    $filter_values = $form_state->cleanValues()->getValue(['filters', 'advanced_search']);

    $asset_results = $this->orangeLogicManager->search(
      $this->orangeLogicManager->buildCriteriaQuery($filter_values),
      [
        'page_number' => $filter_values['pagenumber'],
        'per_page' => $filter_values['countperpage'],
      ]
    );

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#search-results-items', $asset_results->renderItems()));

    return $response;
  }

  /**
   * Prepares the entities without saving them.
   *
   * We need this method when we want to validate or perform other operations
   * before submit.
   *
   * @param array $form
   *   Complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $entities = [];

    /** @var  \Drupal\media_orange_logic\OrangeLogicSearchResponse $results */
    $results = $this->manager->getSelectedAssets($form_state);

    if (!$results) {
      return [];
    }

    $target_media_type = $this->getTargetBundle();

    /** @var \Drupal\media\MediaTypeInterface $media_type */
    $media_type = $this->entityTypeManager
      ->getStorage('media_type')
      ->load($target_media_type);

    $source_field_name = $this->getSourceField();
    $config = $this->getConfiguration();
    foreach ($results->getItems() as $result) {

      $asset_name = $this->orangeLogicResultRenderer->getAssetName($result);
      $name_from_mapping = $result->{$config['field_mapping']['name']};
      $media_entity_definition = [
        'bundle' => $media_type->id(),
        'name' => !empty($name_from_mapping) ? $name_from_mapping : $asset_name,
        'langcode' => $config['default_langcode'],
        $source_field_name => $this->manager->generateMediaSourceFieldValue($result, $media_type),
      ];

      // We fill any Orange Logic field with the metadata, in case of any:
      if (!($media_type->getSource() instanceof OrangeLogicMediaSource)) {
        // Todo: Check if it worx also for orange_logic field type!
        $media_fields = $this->entityFieldManager
          ->getFieldDefinitions('media', $media_type->id());

        /** @var \Drupal\Core\Field\BaseFieldDefinition $media_field */
        foreach ($media_fields as $media_field) {
          if ($media_field->getType() == 'orange_logic_field') {
            $field_name = $media_field->getName();
            $media_entity_definition[$field_name] = $this->manager->generateOrangeLogicFieldValue($result, $media_field);
          }
        }
      }

      // Create a media:
      $media = $this->entityTypeManager
        ->getStorage('media')
        ->create($media_entity_definition);

      $media = $this->setEntityExtraFields($media, $result);

      $entities[] = $media;
    }

    return $entities;
  }

  /**
   * Set extra fields of entity (by mapping from config).
   *
   * @param \Drupal\core\Entity\EntityInterface $entity
   *   Prepared media entity.
   * @param object $result
   *   API response object.
   *
   * @return \Drupal\core\Entity\EntityInterface
   *   Entity with extra fields set.
   */
  protected function setEntityExtraFields(EntityInterface $entity, object $result) {
    $config = $this->getConfiguration();

    foreach ($config['field_mapping'] as $field_name => $value) {
      if (empty($result->$value)) {
        continue;
      }

      $result_value = $result->$value;
      $field_definition = $this->entityFieldManager->getFieldStorageDefinitions($entity->getEntityTypeId());
      $max_length = $field_definition[$field_name]->getSetting('max_length');

      if ($max_length && mb_strlen($result_value) > $max_length) {
        $result_value = mb_substr($result_value, 0, $max_length);
      }

      $entity->set($field_name, $result_value);
    }

    return $entity;

  }

}
