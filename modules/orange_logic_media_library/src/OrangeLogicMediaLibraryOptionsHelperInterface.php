<?php

namespace Drupal\orange_logic_media_library;

/**
 * Contains OrangeLogicMediaLibraryOptionsHelperInterface.
 */
interface OrangeLogicMediaLibraryOptionsHelperInterface {

  /**
   * Get color options for advanced search.
   *
   * @return array
   *   Color options.
   */
  public function getColorOptions();

  /**
   * Get per page select options for advanced search.
   *
   * @return array
   *   Per page amount options.
   */
  public function getPerPageOptions();

  /**
   * Get orientation options for advanced search.
   *
   * @return array
   *   Orientation options.
   */
  public function getOrientationOptions();

  /**
   * Mapping field options.
   *
   * @return array
   *   Mapping options for the fields.
   */
  public function getMappingOptions();

  /**
   * Get langcode options for config.
   *
   * @return array
   *   Lancode options.
   */
  public function getLangcodeOptions();

  /**
   * Mapping field names.
   *
   * @return array
   *   Field names for the mapping.
   */
  public function getMappingFieldNames();

}
