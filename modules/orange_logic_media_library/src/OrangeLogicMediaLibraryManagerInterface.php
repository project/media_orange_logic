<?php

namespace Drupal\orange_logic_media_library;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\media\MediaTypeInterface;

/**
 * Contains OrangeLogicMediaLibraryManagerInterface.
 */
interface OrangeLogicMediaLibraryManagerInterface {

  /**
   * Save entities to database.
   *
   * @param \Drupal\core\Entity\EntityInterface[] $prepared_entities
   *   Prepared entities.
   *
   * @return array
   *   Saved entities.
   */
  public function saveEntities(array $prepared_entities);

  /**
   * Retrieves the selected media to be imported.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return \Drupal\media_orange_logic\OrangeLogicSearchResponse|bool
   *   The selected media data taken from the Organge Server server or FALSE.
   */
  public function getSelectedAssets(FormStateInterface $form_state);

  /**
   * Generate the value for the Media Source field.
   *
   * If the source is OrangeLogicMediaSource we fill the meta information
   * from OL.
   *
   * @param object $result
   *   The asset data from OL.
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type to get the media source and generate a value.
   *
   * @return array
   *   The values array with the data required by the field used as source for
   *   the given $media_type.
   */
  public function generateMediaSourceFieldValue($result, MediaTypeInterface $media_type);

  /**
   * Generates the required array of values by Orange Logic field type.
   *
   * The data is based on the $result variable. If the field is configured as
   * "Video" or "Audio" fill their dedicated metadata. It doesn't in other case.
   *
   * @param object $result
   *   The asset data from OL.
   * @param \Drupal\field\Entity\FieldConfig $source_field_definition
   *   The field config definition of the Orange Logic field type.
   *
   * @return array
   *   The array of values with the Asset metadata.
   */
  public function generateOrangeLogicFieldValue($result, FieldConfig $source_field_definition);

}
