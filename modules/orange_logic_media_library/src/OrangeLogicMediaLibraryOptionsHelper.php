<?php

namespace Drupal\orange_logic_media_library;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\media_orange_logic\OrangeLogicManager;

/**
 * Contains OrangeLogicMediaLibraryOptionsHelper class.
 */
class OrangeLogicMediaLibraryOptionsHelper implements OrangeLogicMediaLibraryOptionsHelperInterface {

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * OrangeLogicMediaLibraryOptionsHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity Field Manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language Manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, LanguageManagerInterface $language_manager) {
    $this->entityFieldManager = $entity_field_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function getColorOptions() {
    return [
      'all' => t('All'),
      'True' => t('Color pictures'),
      'False' => t('Black and White pictures'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getPerPageOptions() {
    $per_page = ['10', '25', '50', '100'];
    return array_combine($per_page, $per_page);
  }

  /**
   * {@inheritDoc}
   */
  public function getOrientationOptions() {
    return [
      'all' => t('All'),
      'Landscape' => t('Landscape'),
      'Portrait' => t('Portrait'),
      'Square' => t('Square'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getMappingOptions() {
    $response_fields = OrangeLogicManager::DEFAULT_FIELDS;

    return array_merge(
      ['' => t('None')],
      array_combine($response_fields, $response_fields)
    );

  }

  /**
   * {@inheritDoc}
   */
  public function getLangcodeOptions() {
    $langcode_options = ['' => t('None')];
    foreach ($this->languageManager->getLanguages() as $lang) {
      $langcode_options[$lang->getId()] = $lang->getName();
    }

    return $langcode_options;
  }

  /**
   * {@inheritDoc}
   */
  public function getMappingFieldNames($target_entity_type = 'media') {
    $string = $this->entityFieldManager->getFieldMapByFieldType('string');
    $string_long = $this->entityFieldManager->getFieldMapByFieldType('string_long');
    $text = $this->entityFieldManager->getFieldMapByFieldType('text');
    $text_long = $this->entityFieldManager->getFieldMapByFieldType('text_long');
    $text_with_summary = $this->entityFieldManager->getFieldMapByFieldType('text_with_summary');

    $fields = array_merge_recursive(
      isset($string[$target_entity_type]) ? $string[$target_entity_type] : [],
      isset($string_long[$target_entity_type]) ? $string_long[$target_entity_type] : [],
      isset($text[$target_entity_type]) ? $text[$target_entity_type] : [],
      isset($text_long[$target_entity_type]) ? $text_long[$target_entity_type] : [],
      isset($text_with_summary[$target_entity_type]) ? $text_with_summary[$target_entity_type] : []
    );

    $fields = array_filter($fields, function ($element) {
      return isset($element['bundles']['image']);
    }, ARRAY_FILTER_USE_BOTH);

    if (isset($fields['revision_log_message'])) {
      unset($fields['revision_log_message']);
    }

    return array_keys($fields);

  }

}
