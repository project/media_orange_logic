<?php

namespace Drupal\orange_logic_media_library;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\file\FileRepositoryInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media_orange_logic\OrangeLogicManagerInterface;
use Drupal\media_orange_logic\OrangeLogicResultRendererInterface;
use Drupal\media_orange_logic\Plugin\media\Source\OrangeLogicMediaSource;

/**
 * Contains OrangeLogicMediaLibraryManager class.
 */
class OrangeLogicMediaLibraryManager implements OrangeLogicMediaLibraryManagerInterface {

  /**
   * OrangeLogicMediaLibraryManager constructor.
   *
   * @param \Drupal\media_orange_logic\OrangeLogicManagerInterface $orange_logic_manager
   *   Orange Logic Manager.
   * @param \Drupal\media_orange_logic\OrangeLogicResultRendererInterface $orange_logic_result_renderer
   *   Orange Logic Result Renderer Service.
   * @param \Drupal\file\FileRepositoryInterface $fileRepository
   *   File repository.
   */
  public function __construct(
    protected OrangeLogicManagerInterface $orangeLogicManager,
    protected OrangeLogicResultRendererInterface $orangeLogicResultRenderer,
    protected FileRepositoryInterface $fileRepository
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public function getSelectedAssets(FormStateInterface $form_state) {
    // Todo: why doesn't it work?
    // $selected_assets = $form_state->getValue('selected_assets');.
    $selected_assets = array_column(
      $form_state->getValues(),
      'selected_media'
    );

    if (empty($selected_assets[0]['selected_assets'])) {
      return FALSE;
    }

    $selected_assets = $selected_assets[0]['selected_assets'];

    $query = [
      'SystemIdentifier' => explode(',', $selected_assets),
    ];

    return $this->orangeLogicManager->search(
      $this->orangeLogicManager->buildCriteriaQuery($query)
    );
  }

  /**
   * {@inheritDoc}
   */
  public function generateMediaSourceFieldValue($result, MediaTypeInterface $media_type) {
    $media_source = $media_type->getSource();

    if ($media_source instanceof OrangeLogicMediaSource) {
      // Todo: check if it worx with orange logic field!
      $source_field_definition = $media_source->getSourceFieldDefinition($media_type);
      return $this->generateOrangeLogicFieldValue($result, $source_field_definition);
    }
    else {
      $asset_source = $this->orangeLogicResultRenderer->getAssetMediaSource($result);
      $asset_filename = $this->orangeLogicResultRenderer->getAssetFileName($result);

      // Create the file:
      $asset = file_get_contents($asset_source);
      $file = $this->fileRepository->writeData(
        $asset,
        'public://' . $asset_filename,
        FileSystemInterface::EXISTS_REPLACE
      );

      return [
        'target_id' => $file->id(),
        'alt' => $this->orangeLogicResultRenderer->getAssetName($result),
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function generateOrangeLogicFieldValue($result, FieldConfig $source_field_definition) {
    // Fill the common media metadata:
    $media_source_field_value = [
      'system_id' => !empty($result->SystemIdentifier) ? $result->SystemIdentifier : '',
      'title' => !empty($result->Title) ? $result->Title : '',
      'original_title' => !empty($result->ParentFolderTitle) ? $result->ParentFolderTitle : '',
      'caption' => !empty($result->Caption) ? $result->Caption : '',
      'caption_long' => !empty($result->CaptionLong) ? $result->CaptionLong : '',
      'artist' => !empty($result->Artist) ? $result->Artist : '',
      'media_type' => !empty($result->MediaType) ? $result->MediaType : '',
      'copyright' => !empty($result->copyright) ? $result->copyright : '',
      'max_width' => !empty($result->MaxWidth) ? $result->MaxWidth : '',
      'max_height' => !empty($result->MaxHeight) ? $result->MaxHeight : '',
    ];

    // Fill the media date metadata:
    if (!empty($result->MediaDate)) {
      $media_source_field_value['media_date'] = strtotime($result->MediaDate);
    }

    return $media_source_field_value;
  }

  /**
   * {@inheritDoc}
   */
  public function saveEntities(array $prepared_entities) {
    $saved_entities = [];
    foreach ($prepared_entities as $key => $prepared_entity) {
      if ($prepared_entity->isNew()) {
        $prepared_entity->save();
        $saved_entities[$prepared_entity->id()] = $prepared_entity;
      }
    }
    return $saved_entities;
  }

}
