/**
 * @file
 * Attaches the behaviors for the Media Orange Logic Entity Browser UI.
 */

(function($, Drupal, once) {
  Drupal.behaviors.ebOrangeLogic = {

    /**
     * Refreshes the selected media layer with the set of selected assets.
     */
    refreshSelectedAssets: function (context) {
      const selectedAssets = Drupal.behaviors.ebOrangeLogic.getSelectedAssetsInput();

      if (selectedAssets.length > 0) {
        $.ajax({
          url: '/media-orange-logic/eb/ajax/selected-assets',
          type: 'POST',
          data: { 'asset_ids[]': selectedAssets },
          dataType: 'json',
          success: function (data) {
            if (data) {
              $('#selected-media-items', context).html(data);
            }
          },
        });
      }
      else {
        const emptyMarkup = Drupal.t('No assets have been chosen. Please click on a asset to add it to the "Selected assets" section');
        $('#selected-media-items', context).html(emptyMarkup);
      }
    },

    /**
     * Adds to the set of selected assets the given element.
     *
     * @param element
     *   The element to be added.
     */
    addAsset: function (element) {
      const selectedAssets = Drupal.behaviors.ebOrangeLogic.getSelectedAssetsInput();
      const asset_id = element.attr('data-systemId');

      selectedAssets.push(asset_id);
      Drupal.behaviors.ebOrangeLogic.setSelectedAssetsInput(selectedAssets);
    },

    /**
     * Removes from the set of selected assets the given element.
     *
     * @param element
     *   The element to be removed.
     */
    removeAsset: function (element) {
      const selectedAssets = Drupal.behaviors.ebOrangeLogic.getSelectedAssetsInput();
      const asset_id = element.attr('data-systemId');

      const index = selectedAssets.indexOf(asset_id);
      if (index >= 0) {
        selectedAssets.splice(index, 1);
        Drupal.behaviors.ebOrangeLogic.setSelectedAssetsInput(selectedAssets);
      }
    },

    /**
     * Retrieves the array of asset ids from the input tag.
     *
     * @returns {string[] | *}
     *   The strings array with the asset ids.
     */
    getSelectedAssetsInput: function () {
      let selectedAssets = $('input#selected-assets').val();
      // We initialize the selected assets storage in case it is empty.
      if (selectedAssets === '') {
        selectedAssets = [];
      }
      else {
        selectedAssets = selectedAssets.split(",");
      }
      console.log(selectedAssets);
      return selectedAssets;
    },

    /**
     * Updates the input which is used to store the selected assets.
     */
    setSelectedAssetsInput: function (assetIds) {
      $('input#selected-assets').val(assetIds.join());
    },

    attach: function (context, settings) {
      // If we click on a asset we add it to "selected assets":
      $(once('ebOrangeLogic', '#search-results-items .media-item', context)).on('click', function (e) {

        const $this = $(this);

        Drupal.behaviors.ebOrangeLogic.addAsset($this);
        Drupal.behaviors.ebOrangeLogic.refreshSelectedAssets();
        // Scroll to the section with the selected assets.
        document.querySelector('[data-drupal-selector="edit-filters-selected-media"]').scrollIntoView({behavior: 'smooth'});;
      });

      // If we click on a selected asset we remove it from "selected assets":
      $(once('ebOrangeLogic', document.body, context)).on('click', '#selected-media-items .media-item', function (e) {

        const $this = $(this);

        Drupal.behaviors.ebOrangeLogic.removeAsset($this);
        Drupal.behaviors.ebOrangeLogic.refreshSelectedAssets();
      });

      once('ebOrangeLogic', '.media-orage-logic-next-button', context).forEach(element => {
        element.addEventListener('click', (event) => {
          event.preventDefault();
          const pageNumber = document.querySelector('input[data-drupal-selector="edit-filters-advanced-search-pagenumber"]');
          pageNumber.value++;

          event.target.closest("form").querySelector("input[type=submit]").dispatchEvent(new Event('mousedown'));

          const currentPage = document.querySelector('input[name="filters[search_results][pager][current_page]"]');
          currentPage.value++;
          if (parseInt(currentPage.value) > 1) {
            document.querySelector('.media-orage-logic-prev-button').classList.remove('visually-hidden');
          }
          return false;
        });
      });

      once('ebOrangeLogic', '.media-orage-logic-prev-button', context).forEach(element => {
        element.addEventListener('click', (event) => {
          event.preventDefault();
          const pageNumber = document.querySelector('input[data-drupal-selector="edit-filters-advanced-search-pagenumber"]');
          pageNumber.value--;

          event.target.closest("form").querySelector("input[type=submit]").dispatchEvent(new Event('mousedown'));

          const currentPage = document.querySelector('input[name="filters[search_results][pager][current_page]"]');
          currentPage.value--;
          if (parseInt(currentPage.value) <= 1) {
            document.querySelector('.media-orage-logic-prev-button').classList.add('visually-hidden');
          }

          return false;
        });
      });
    },
  };
}(jQuery, Drupal, once));
