# README
# Media Orange Logic (1.x)

## Installation

Module dependencies contain some patches. Thus install via composer only.

Patches to add to your root composer.json (for media_library_extend 2.1.0-alpha1):
```
"drupal/media_library_extend": {
    "3218930 - Pass form, form_state to getResults method": "https://www.drupal.org/files/issues/2021-06-15/3218930_media_library_extend_1.patch",
    "3218930 - Remove Media library extend samples as they are incompatible with the patch above": "https://www.drupal.org/files/issues/2024-02-01/media_library_extend_remove_samples-6.patch",
    "Change MLE return value": "https://www.drupal.org/files/issues/2021-06-22/mle_change_return_value_0.patch"
},
```


## Configuration

First provide credentials for API endpoint.
**/admin/config/media/media-orange-logic**

### Entity Browser

- Add Entity Reference field for type media to your content type.
- Manage Form Display:
  select _Entity Browser_ widget here, select _OrangeLogicMediaBrowser_ entity browser.
- You can add a field of the type _Orange Logic_ to your media type.
  The whole image data retrieved from the DAM resource will be stored there.

#### ToDo:
 - Now all media types are being searched, even those which are not allowed in field settings.
 - Autofill of extra fields in media entity (alt-text etc.) is not supported yet.

### Media Library

- Enable _Orange Logic Media Library_ submodule.
- Add Entity Reference field for type media to your content type.
- Manage Form Display: select _Media Library_ widget here.
- Add a media library pane with a mapping here
**/admin/config/media/media-library/pane**
  Mapping is used to fill extra fields of media entity (not supported by entity browser widget)
- Try to add media to your node via media library using created pane.

#### ToDo

- Only images are supported by Media Library Orange Logic widget.
  Audio/Video is not supported yet.

### Notes

- This module is now under development. Very big part of the features is still raw.
