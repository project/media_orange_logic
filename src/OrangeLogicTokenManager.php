<?php

namespace Drupal\media_orange_logic;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use SimpleXMLElement;

/**
 * Class OrangeLogicTokenManager.
 */
class OrangeLogicTokenManager implements OrangeLogicTokenManagerInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The ConfigFactory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The token string.
   *
   * @var string
   */
  protected $token;

  /**
   * The expirantion time for a token taken from the service.
   *
   * @var int
   */
  protected $expirationTime;

  /**
   * Constructs a new OrangeLogicTokenManager object.
   */
  public function __construct(
    ClientInterface $http_client,
    ConfigFactoryInterface $config_factory,
    PrivateTempStoreFactory $temp_store_private
  ) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->tempStore = $temp_store_private;

    if (!empty($this->tempStore->get('orange_logic:token'))) {
      $orange_logic_temp_store = $this->tempStore->get('orange_logic:token');
      $this->token = $orange_logic_temp_store->get('token');
      $this->expirationTime = $orange_logic_temp_store->get('expirationTime');
    }

  }

  /**
   * Retrieves a valid token.
   *
   * If the current stored token is not valid. It requests a new one to the
   * service.
   *
   * @return string
   *   A valid token string.
   */
  public function getToken() : string {
    if (!$this->exists() || !$this->isValid()) {
      $this->refresh();
    }

    return $this->token;
  }

  /**
   * Checks if there is any token already on place.
   *
   * @return bool
   *   TRUE if exists a token which was calculated previously.
   */
  protected function exists() : bool {
    return !empty($this->token);
  }

  /**
   * Check if the current token is still valid according to its expiration time.
   *
   * @return bool
   *   TRUE if the expirationTime is older or equal to the current time.
   */
  protected function isValid() : bool {
    $is_valid = FALSE;

    if ($this->exists() && !empty($this->expirationTime)) {
      $is_valid = time() <= $this->expirationTime;
    }

    return $is_valid;
  }

  /**
   * Refresh the token string.
   *
   * It requests a new token string from the service. It uses the credentials
   * set at the module's administration page.
   */
  protected function refresh() : void {
    // Get the config set at the administration page:
    $config = $this->configFactory->get('media_orange_logic.settings');

    $token_endpoint = $config->get('token_endpoint');

    if (empty($token_endpoint)) {
      // ToDo: check if config is set properly.
    }

    try {
      $response = $this->httpClient->request(
        'POST',
        $token_endpoint,
        [
          'query' => [
            'Login' => $config->get('username'),
            'Password' => $config->get('password'),
          ],
        ]
      );
    }
    catch (GuzzleException $e) {
      // @TODO : Log the error here.
    }

    if (!empty($response)) {
      $data = new SimpleXMLElement($response->getBody()->getContents());

      // The configured expired time. This period of time is in minutes.
      $timeout = (integer) $data->APIRequestInfo->TimeoutPeriodMinutes[0];

      $this->token = reset($data->APIResponse->Token);
      $this->expirationTime = time() + ($timeout * 60);

      // Store into the tempStore the refreshed token to have is accessible:
      $orange_logic_temp_store = $this->tempStore->get('orange_logic:token');
      $orange_logic_temp_store->set('token', $this->token);
      $orange_logic_temp_store->set('expirationTime', $this->expirationTime);
    }

  }

}
