<?php

namespace Drupal\media_orange_logic;

/**
 * Interface OrangeLogicRendererInterface.
 */
interface OrangeLogicResultRendererInterface {

  /**
   * Renders a Search Result in the given $format.
   *
   * @param object $data
   *   The search result data.
   * @param string $format
   *   The format on which it is desired to render the media.
   *
   * @return array
   *   The rendereable array with the representation of the media resource
   *   using the given format.
   */
  public function render(object $data, string $format);

}
