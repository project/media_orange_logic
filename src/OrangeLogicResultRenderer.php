<?php

namespace Drupal\media_orange_logic;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Class OrangeLogicResultRenderer.
 */
class OrangeLogicResultRenderer implements OrangeLogicResultRendererInterface {

  /**
   * The thumbnail format name for teasers.
   */
  const TEASER_FORMAT = 'thumbnail';

  /**
   * The full format name for detailed formats.
   */
  const FULL_FORMAT = 'full';

  /**
   * The DateFormatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The ModuleExtensionList service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs a new OrangeLogicResultRenderer object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The DateFormatterInterface service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The ModuleExtensionList service.
   */
  public function __construct(
    DateFormatterInterface $date_formatter,
    ModuleExtensionList $module_extension_list
  ) {
    $this->dateFormatter = $date_formatter;
    $this->moduleExtensionList = $module_extension_list;
  }

  /**
   * {@inheritDoc}
   */
  public function render(object $data, string $format = self::TEASER_FORMAT) {
    switch ($format) {
      case self::TEASER_FORMAT:
        $renderer = $this->renderThumbnail($data);
        break;

      case self::FULL_FORMAT:
        $renderer = $this->renderFull($data);
        break;

      default:
        $renderer = [];
        break;
    }

    return $renderer;
  }

  /**
   * Renders an Orange Logic source in thumbnail format.
   *
   * @param object $data
   *   The search result data.
   *
   * @return array
   *   The rendereable array with the representation of the media resource
   *   using the thumbnail format.
   */
  protected function renderThumbnail(object $data) {
    switch ($data->MediaType) {
      case 'Image':
        $thumbnail = $this->renderImage($data, self::TEASER_FORMAT);
        break;

      case 'Video':
        $thumbnail = $this->renderVideo($data, self::TEASER_FORMAT);
        break;

      case 'Audio':
        $thumbnail = $this->renderAudio($data, self::TEASER_FORMAT);
        break;

      default:
        $thumbnail = [];
        break;
    }

    $metadata = [];
    $metadata['uid'] = isset($data->SystemIdentifier) ? $data->SystemIdentifier : '';
    $metadata['date'] = !empty($data->MediaDate) ? $this->dateFormatter->format(strtotime($data->MediaDate), 'custom', 'd/m/Y') : '';
    $metadata['country'] = isset($data->Country) ? $data->Country : '';
    $metadata['title'] = $this->getAssetName($data);
    $metadata['artist'] = isset($data->Artist) ? $data->Artist : '';
    $metadata['mediaType'] = isset($data->MediaType) ? $data->MediaType : '';

    return [
      '#theme' => 'media_orange_logic_thumbnail',
      '#original_asset' => $data,
      '#metadata' => $metadata,
      '#thumbnail' => $thumbnail,
    ];
  }

  /**
   * Renders an Orange Logic source in full format.
   *
   * @param object $data
   *   The search result data.
   *
   * @return array
   *   The rendereable array with the representation of the media resource
   *   using the full format.
   */
  protected function renderFull(object $data) {
    // @TODO : Implement this method.
    return [];
  }

  /**
   * Renders an Orange Logic Image mediaType in with the given $format.
   *
   * @param object $data
   *   The search result data.
   * @param string $format
   *   The format on which it is desired to render the media.
   *
   * @return array
   *   The rendereable array with the representation of the Image mediaType
   *   using the given $format.
   */
  protected function renderImage(object $data, string $format) {
    return [
      '#theme' => 'image',
      '#uri' => $this->getImageMediaSource($data, $format),
      '#attributes' => [
        'data-target' => $data->SystemIdentifier,
      ],
    ];
  }

  /**
   * Renders an Orange Logic Video mediaType in with the given $format.
   *
   * @param object $data
   *   The search result data.
   * @param string $format
   *   The format on which it is desired to render the media.
   *
   * @return array
   *   The rendereable array with the representation of the Video mediaType
   *   using the given $format.
   */
  protected function renderVideo(object $data, string $format) {
    return [
      '#theme' => 'image',
      '#uri' => $this->moduleExtensionList->getPath('media_orange_logic') . '/images/video-x-generic.png',
      '#attributes' => [
        'data-target' => $data->SystemIdentifier,
      ],
    ];
  }

  /**
   * Renders an Orange Logic Audio mediaType in with the given $format.
   *
   * @param object $data
   *   The search result data.
   * @param string $format
   *   The format on which it is desired to render the media.
   *
   * @return array
   *   The rendereable array with the representation of the Audio mediaType
   *   using the given $format.
   */
  protected function renderAudio(object $data, $format) {
    return [
      '#theme' => 'image',
      '#uri' => $this->moduleExtensionList->getPath('media_orange_logic') . '/images/audio-x-generic.png',
      '#attributes' => [
        'data-target' => $data->SystemIdentifier,
      ],
    ];
  }

  /**
   * Retrieves the source of the Asset depending on MediaType.
   *
   * @param object $data
   *   The search result data.
   *
   * @return string
   *   The source of the Asset based on the MediaType.
   */
  public function getAssetMediaSource(object $data) : string {
    switch ($data->MediaType) {
      case 'Image':
        $source = $this->getImageMediaSource($data, self::TEASER_FORMAT);
        break;

      case 'Video':
        $source = $this->getVideoMediaSource($data, self::TEASER_FORMAT);
        break;

      case 'Audio':
        $source = $this->getAudioMediaSource($data, self::TEASER_FORMAT);
        break;

      default:
        $source = '';
        break;
    }

    return $source;
  }

  /**
   * Retrieves the source of the Image depending on the desired format.
   *
   * As fallback it retrieves the high-resolution format (Path_TR1).
   *
   * @param object $data
   *   The search result data.
   * @param string $format
   *   The format on which it is desired to get the image source.
   *
   * @return mixed
   *   The source of the Image based on the desired $format. As fallback
   *   it is retrieved the source Path_TR1.
   */
  public function getImageMediaSource(object $data, string $format) {
    // By default we get the High resolution as fallback format.
    $fallback_source = !empty($data->Path_CMS1->URI)
      ? $data->Path_CMS1->URI
      : $data->Path_TR1->URI;

    switch ($format) {
      case self::TEASER_FORMAT:
        $source = empty($data->Path_TR6->URI) ? $fallback_source : $data->Path_TR6->URI;
        break;

      default:
        $source = $fallback_source;
    }

    return $source;
  }

  /**
   * Retrieves the asset source path from a OL audio asset.
   *
   * It takes the info from the Path_Web->URI attribute.
   *
   * @param object $data
   *   The asset data from the service.
   *
   * @return string
   *   The source string from the asset.
   */
  protected function getAudioMediaSource(object $data) : string {
    return !empty($data->Path_Web->URI) ? $data->Path_Web->URI : '';
  }

  /**
   * Retrieves the asset source path from a OL video asset.
   *
   * It takes the info from the  attribute.
   *
   * @param object $data
   *   The asset data from the service.
   *
   * @return string
   *   The source string from the asset.
   */
  protected function getVideoMediaSource(object $data) : string {
    return !empty($data->Path_WebHigh->URI) ? $data->Path_WebHigh->URI : '';
  }

  /**
   * Gets the asset name from the asset data information.
   *
   * It gets the asset name based on the SystemIdentifier.
   *
   * @param object $data
   *   The Object with the asset result.
   *
   * @return string
   *   The asset name taken from the $data source.
   */
  public function getAssetName(object $data) : string {
    $asset_name = '';

    // First option is to use the Title:
    if (!empty($data->Title)) {
      $asset_name = $data->Title;
    }
    // If there is not any Title available we try with the ParentFolderTitle:
    elseif (!empty($data->ParentFolderTitle)) {
      $asset_name = $data->ParentFolderTitle;
    }
    // As fallback we use the SystemIdentifier.
    elseif ($data->SystemIdentifier) {
      $asset_name = $data->SystemIdentifier;
    }

    return $asset_name;
  }

  /**
   * Gets the asset file name from the asset data information.
   *
   * @param object $data
   *   The Object with the asset result.
   * @param string $media_url
   *   The media url to use to generate the filename.
   *
   * @return string
   *   The filename taken from the $data source.
   */
  public function getAssetFileName(object $data, string $media_url = '') : string {
    $matches = [];

    // If the media url is empty, we get the media source.
    if (empty($media_url)) {
      $media_url = $this->getAssetMediaSource($data);
    }

    preg_match(
      '/([a-z]|[A-Z]|[0-9])+\.(jpg|gif|png|mp3|mp4)/',
      $media_url,
      $matches
    );

    return empty($matches) ? '' : array_shift($matches);
  }

}
