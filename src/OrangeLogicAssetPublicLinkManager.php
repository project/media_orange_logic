<?php

namespace Drupal\media_orange_logic;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class OrangeLogicAssetPublicLinkManager.
 */
class OrangeLogicAssetPublicLinkManager implements OrangeLogicAssetPublicLinkManagerInterface {

  /**
   * The LogAccess attribute value required by the API.
   */
  const LOG_ACCESS = 1;

  /**
   * The StickToCurrentVersion attribute value required by the API.
   */
  const STICK_TO_CURRENT_VERSION = 0;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The OrangeLogicTokenManagerInterface service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The ConfigFactory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new OrangeLogicAssetPublicLinkManager object.
   *
   * @param \Drupal\media_orange_logic\OrangeLogicTokenManagerInterface $token_manager
   *   The OrangeLogicTokenManagerInterface service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The ClientInterface service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The ConfigFactory service.
   */
  public function __construct(OrangeLogicTokenManagerInterface $token_manager, ClientInterface $http_client, ConfigFactoryInterface $config_factory) {
    $this->tokenManager = $token_manager;
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
  }

  /**
   * Retrieves the Asset public link from the Orange Logic service.
   *
   * @param string $systemId
   *   The asset SystemIdentifier.
   * @param string $format
   *   The format.
   *   It can be 'WebHigh' or 'Web'.
   * @param string $default_url
   *   The default url to be used as default case.
   * @param int $download
   *   The download param.
   *
   * @return string
   *   The public url of the requested asset.
   */
  public function getPublicLink(string $systemId, string $format, string $default_url, int $download = 0) : string {
    $assetUrl = $default_url;
    $config = $this->configFactory->get('media_orange_logic.settings');

    $create_format_link_endpoint = $config->get('create_format_link_endpoint');

    if (empty($create_format_link_endpoint)) {
      // ToDo: check if config is set properly.
    }

    try {
      $response = $this->httpClient->request(
        'GET',
        $create_format_link_endpoint,
        [
          'query' => [
            'token' => $this->tokenManager->getToken(),
            'Identifier' => $systemId,
            'CreateDownloadLink' => $download,
            'StickToCurrentVersion' => self::STICK_TO_CURRENT_VERSION,
            'AssetFormat' => $format,
            'LogAccess' => self::LOG_ACCESS,
            'format' => 'json',
          ],
        ]
      );
    }
    catch (GuzzleException $e) {
      // @TODO : Log the error here.
    }

    if (!empty($response)) {
      $response = json_decode($response->getBody()->getContents());
      if (isset($response->APIResponse->Response->Link)) {
        $assetUrl = $response->APIResponse->Response->Link;
      }
    }

    return $assetUrl;
  }

}
