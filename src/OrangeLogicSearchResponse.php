<?php

namespace Drupal\media_orange_logic;

/**
 * Class OrangeLogicSearchResponse.
 */
class OrangeLogicSearchResponse {

  /**
   * The thumbnail format name for teasers.
   */
  const TEASER_FORMAT = 'thumbnail';

  /**
   * The array of items received from the service.
   *
   * @var array
   */
  protected $items;

  /**
   * The stdClass with the APIRequestInfo data.
   *
   * @var Object
   */
  protected $apiRequestInfo;

  /**
   * The stdClass with the APIResponse->GlobalInfo data.
   *
   * @var Object
   */
  protected $globalInfo;

  /**
   * OrangeLogicSearchResponse constructor.
   *
   * @param string $data
   *   The raw response data from the service.
   *
   * @throws \Exception
   */
  public function __construct(string $data) {
    $decoded_response = json_decode($data);

    if (is_null($decoded_response)) {
      throw new \Exception('Fatal error trying to decode the given data: ' . $data);
    }

    $this->apiRequestInfo = $decoded_response->APIRequestInfo;
    $this->globalInfo = $decoded_response->APIResponse->GlobalInfo;
    $this->items = $decoded_response->APIResponse->Items;
  }

  /**
   * Checks if the response has results.
   *
   * @return bool
   *   TRUE if there are available some results.
   */
  public function hasResults() : bool {
    return isset($this->globalInfo->totalCount) ? ($this->globalInfo->totalCount > 0) : FALSE;
  }

  /**
   * Retrieves a render array with the list of items.
   *
   * @return array
   *   The render array of items to be rendered.
   */
  public function renderItems() : array {
    return [
      '#theme' => 'media_orange_logic_browser',
      '#rows' => $this->getItems(),
      '#general_info' => $this->globalInfo,
    ];
  }

  /**
   * Retrieves the list of items.
   *
   * @return array
   *   The list of item objects.
   */
  public function getItems() : array {
    return $this->items;
  }

}
