<?php

namespace Drupal\media_orange_logic\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MediaOrangeLogicAdminForm.
 */
class MediaOrangeLogicAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_orange_logic.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_orange_logic_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_orange_logic.settings');

    $form['search_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search API Endpoint'),
      '#placeholder' => 'https://dam-example.com/API/search/v3.0/search',
      '#default_value' => $config->get('search_endpoint'),
      '#description' => $this->t('The search entry point at the server.'),
      '#required' => TRUE,
    ];

    $form['token_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token API Endpoint'),
      '#placeholder' => 'https://dam-example.com/API/Authentication/v1.0/Login',
      '#default_value' => $config->get('token_endpoint'),
      '#description' => $this->t('The auth entry point at the server.'),
      '#required' => TRUE,
    ];

    $form['create_format_link_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create Format Link API Endpoint'),
      '#placeholder' => 'https://dam-example.com/API/AssetLink/V1.0/CreateFormatLink',
      '#default_value' => $config->get('create_format_link_endpoint'),
      '#description' => $this->t('The endpoint to get the public Urls for assets. Needed for audio and video only.'),
      '#required' => TRUE,
    ];

    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('System credentials'),
    ];

    $form['credentials']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('username'),
      '#description' => $this->t('The username to get logged in.'),
    ];

    $form['credentials']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('password'),
      '#description' => $this->t('The password.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('media_orange_logic.settings')
      ->set('create_format_link_endpoint', $form_state->getValue('create_format_link_endpoint'))
      ->set('token_endpoint', $form_state->getValue('token_endpoint'))
      ->set('search_endpoint', $form_state->getValue('search_endpoint'))
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->save();
  }

}
