<?php

namespace Drupal\media_orange_logic\Plugin\EntityBrowser\Widget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\entity_browser\WidgetBase;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\field\Entity\FieldConfig;
use Drupal\media\MediaTypeInterface;
use Drupal\media_orange_logic\OrangeLogicAssetPublicLinkManagerInterface;
use Drupal\media_orange_logic\OrangeLogicManagerInterface;
use Drupal\media_orange_logic\OrangeLogicResultRendererInterface;
use Drupal\media_orange_logic\Plugin\media\Source\OrangeLogicMediaSource;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * List medias from Orange Logic service to create medias from the service.
 *
 * @EntityBrowserWidget(
 *   id = "orange_logic_browser_widget",
 *   label = @Translation("Orange Logic Media Browser Selector"),
 *   description = @Translation("Adds selected media from Orange Logic
 *   Service."), auto_select = FALSE
 * )
 */
class OrangeLogicMediaBrowserWidget extends WidgetBase {

  /**
   * The account to use in access checks.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The OrangeLogicResultRenderer service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicResultRendererInterface
   */
  protected $orangeLogicResultRenderer;

  /**
   * The OrangeLogicManagerInterface service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicManagerInterface
   */
  protected $orangeLogicManager;

  /**
   * The OrangeLogicAssetPublicLinkManagerInterface service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicAssetPublicLinkManagerInterface
   */
  protected $orangeLogicAssetPublicLinkManager;

  /**
   * The EntityFieldManager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * WidgetBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account.
   * @param \Drupal\media_orange_logic\OrangeLogicResultRendererInterface $orange_logic_result_renderer
   *   The OrangeLogicResultRendererInterface service.
   * @param \Drupal\media_orange_logic\OrangeLogicManagerInterface $orange_logic_manager
   *   The OrangeLogicManagerInterface service.
   * @param \Drupal\media_orange_logic\OrangeLogicAssetPublicLinkManagerInterface $orange_logic_asset_public_link_manager
   *   The OrangeLogicAssetPublicLinkManagerInterface service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The EntityFieldManagerInterface service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EventDispatcherInterface $event_dispatcher,
    EntityTypeManagerInterface $entity_type_manager,
    WidgetValidationManager $validation_manager,
    AccountInterface $account,
    OrangeLogicResultRendererInterface $orange_logic_result_renderer,
    OrangeLogicManagerInterface $orange_logic_manager,
    OrangeLogicAssetPublicLinkManagerInterface $orange_logic_asset_public_link_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
    $this->account = $account;
    $this->orangeLogicResultRenderer = $orange_logic_result_renderer;
    $this->orangeLogicManager = $orange_logic_manager;
    $this->orangeLogicAssetPublicLinkManager = $orange_logic_asset_public_link_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('current_user'),
      $container->get('media_orange_logic.renderer'),
      $container->get('media_orange_logic.manager'),
      $container->get('media_orange_logic.asset_public_link.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = [];

    foreach ($this->getOrangeLogicMediaTypeOptions() as $key => $value) {
      $default_config['media_type_' . $key] = NULL;
    }

    return $default_config + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $media_type_options = $this->getMediaTypeOptions();
    if (empty($media_type_options)) {
      $url = Url::fromRoute('entity.media_type.add_form')->toString();
      $form['media_type'] = [
        '#markup' => $this->t("You don't have media type of the Orange Logic type. You should <a href='!link'>create one</a>", ['!link' => $url]),
      ];
    }
    else {
      $ol_media_type_options = $this->getOrangeLogicMediaTypeOptions();
      foreach ($ol_media_type_options as $media_option_type => $media_type_label) {
        $form_element_key = 'media_type_' . $media_option_type;
        $form[$form_element_key] = [
          '#type' => 'select',
          '#title' => $this->t('Media type: @type', ['@type' => $media_type_label]),
          '#default_value' => $this->configuration[$form_element_key],
          '#options' => $media_type_options,
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);

    $form['advanced_search'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter criteria'),
      '#collapsible' => TRUE,
    ];

    $form['advanced_search']['Text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Free text search.'),
      '#size' => 40,
      '#maxlength' => 255,
    ];

    $form['advanced_search']['SystemIdentifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('System identifier'),
      '#description' => $this->t('Unique System Identifier for the Media generated by the application.'),
      '#size' => 40,
      '#maxlength' => 255,
    ];

    $media_type_options = $this->getOrangeLogicMediaTypeOptions();
    $form['advanced_search']['MediaType'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Media type'),
      '#options' => $media_type_options,
    ];

    $color_options = $this->getColorOptions();
    $form['advanced_search']['Color'] = [
      '#type' => 'radios',
      '#title' => $this->t('Color'),
      '#description' => $this->t('The picture color. Choose one from the list.'),
      '#options' => $color_options,
      '#default_value' => 'all',
    ];

    $orientation_options = $this->getOrientationOptions();
    $form['advanced_search']['Orientation'] = [
      '#type' => 'radios',
      '#title' => $this->t('Orientation'),
      '#description' => $this->t('The picture orientation. Choose one from the list.'),
      '#options' => $orientation_options,
      '#default_value' => 'all',
    ];

    $form['advanced_search']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Search'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSearchCallback'],
      ],
    ];

    $form['selected_media'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Selected assets'),
      '#collapsible' => TRUE,
    ];

    $form['selected_media']['thumbnails'] = [
      '#markup' => '<div id="selected-media-items">' . $this->t('No chosen assets. Please click on a asset to add it to the "Selected assets" section') . '</div>',
    ];

    // Hidden input to store the selected assets.
    $form['selected_media']['selected_assets'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'selected-assets',
      ],
    ];

    $form['search_results'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search results'),
      '#collapsible' => TRUE,
    ];

    $form['search_results']['results'] = [
      '#markup' => '<div id="search-results-items"></div>',
    ];

    // Add the required libraries.
    $form['#attached']['library'][] = 'media_orange_logic/entity_browser_ui';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $entities = [];

    /** @var  \Drupal\media_orange_logic\OrangeLogicSearchResponse $results */
    $results = $this->getSelectedAssets($form_state);

    foreach ($results->getItems() as $result) {
      /** @var \Drupal\media\MediaTypeInterface $media_type */
      $media_type = $this->getMediaTypeFromAssetType($result);

      if (empty($media_type)) {
        continue;
      }

      $source_field_name = $media_type->getSource()->getConfiguration()['source_field'];
      $asset_name = $this->orangeLogicResultRenderer->getAssetName($result);

      $media_entity_definition = [
        'bundle' => $media_type->id(),
        'name' => $asset_name,
        $source_field_name => $this->generateMediaSourceFieldValue($result, $media_type),
      ];

      // We fill any Orange Logic field with the metadata, in case of any:
      if (!($media_type->getSource() instanceof OrangeLogicMediaSource)) {

        $media_fields = $this->entityFieldManager
          ->getFieldDefinitions('media', $media_type->id());

        /** @var \Drupal\Core\Field\BaseFieldDefinition $media_field */
        foreach ($media_fields as $media_field) {
          if ($media_field->getType() == 'orange_logic_field') {
            $field_name = $media_field->getName();
            $media_entity_definition[$field_name] = $this->generateOrangeLogicFieldValue($result, $media_field);
          }
        }
      }

      // Create a media:
      $media = $this->entityTypeManager
        ->getStorage('media')
        ->create($media_entity_definition);

      $entities[] = $media;
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getTriggeringElement()['#eb_widget_main_submit'])) {

      $prepared_entities = $this->prepareEntities($form, $form_state);

      foreach ($prepared_entities as $key => $prepared_entity) {
        if ($prepared_entities[$key]->isNew()) {
          $prepared_entities[$key]->save();
        }
      }

      $this->selectEntities($prepared_entities, $form_state);
    }
  }

  /**
   * AJAX method to update the search result based on the set values.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormStateInterface object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function ajaxSearchCallback(array &$form, FormStateInterface $form_state) {
    $filter_values = $form_state->cleanValues()->getValues();

    $asset_results = $this->orangeLogicManager->search(
      $this->orangeLogicManager->buildCriteriaQuery($filter_values)
    );

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#search-results-items', $asset_results->renderItems()));

    return $response;
  }

  /**
   * Retrieves the list of allowed media types.
   *
   * @return array
   *   The array of media types allowed.
   */
  protected function getOrangeLogicMediaTypeOptions() : array {
    return [
      'image' => $this->t('Image'),
      'video' => $this->t('Video'),
      'audio' => $this->t('Audio'),
    ];
  }

  /**
   * Retrieves the list of allowed colors according to the API.
   *
   * @return array
   *   The array of color options allowed by the API.
   */
  protected function getColorOptions() : array {
    return [
      'all' => $this->t('All'),
      'True' => $this->t('Color pictures'),
      'False' => $this->t('Black and White pictures'),
    ];
  }

  /**
   * Retrieves the list of allowed orientations according to the API.
   *
   * @return array
   *   The array of orientations options allowed by the API.
   */
  protected function getOrientationOptions() : array {
    return [
      'all' => $this->t('All'),
      'Landscape' => $this->t('Landscape'),
      'Portrait' => $this->t('Portrait'),
      'Square' => $this->t('Square'),
    ];
  }

  /**
   * Retrieves the selected media to be imported.
   *
   * @return \Drupal\media_orange_logic\OrangeLogicSearchResponse
   *   The selected media data taken from the Organge Server server.
   */
  protected function getSelectedAssets(FormStateInterface $form_state) {
    $selected_assets = $form_state->getValue('selected_assets');

    $query = [
      'SystemIdentifier' => explode(',', $selected_assets),
    ];

    return $this->orangeLogicManager->search(
      $this->orangeLogicManager->buildCriteriaQuery($query)
    );
  }

  /**
   * Retrieves the list of Media Bundles defined at the system.
   *
   * @return array
   *   The array of media bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getMediaTypeOptions() : array {
    $media_type_options = [];

    $media_types = $this->entityTypeManager
      ->getStorage('media_type')
      ->loadByProperties([
        'source' => [
          'orange_logic_source',
          'image',
          'audio_file',
        ],
      ]);

    /** @var \Drupal\media\Entity\MediaType $media_type */
    foreach ($media_types as $media_type) {
      $media_type_options[$media_type->id()] = $media_type->label();
    }

    return $media_type_options;
  }

  /**
   * Retrieves a configured Media type for given MediaType from OL.
   *
   * @param object $result
   *   The asset data from OL.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The MediaType Config Entity.
   */
  protected function getMediaTypeFromAssetType(object $result) {
    $media_type = NULL;

    $ol_media_type = strtolower($result->MediaType);
    if (!empty($this->configuration['media_type_' . $ol_media_type])) {
      try {
        $media_type = $this->entityTypeManager
          ->getStorage('media_type')
          ->load($this->configuration['media_type_' . $ol_media_type]);
      }
      catch (\Exception $exception) {
        // @TODO Log the error here.
      }
    }

    return $media_type;
  }

  /**
   * Generate the value for the Media Source field.
   *
   * If the source is OrangeLogicMediaSource we fill the meta information
   * from OL.
   *
   * @param object $result
   *   The asset data from OL.
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type to get the media source and generate a value.
   *
   * @return array
   *   The values array with the data required by the field used as source for
   *   the given $media_type.
   */
  protected function generateMediaSourceFieldValue(object $result, MediaTypeInterface $media_type) {
    $media_source = $media_type->getSource();

    if ($media_source instanceof OrangeLogicMediaSource) {
      $source_field_definition = $media_source->getSourceFieldDefinition($media_type);
      return $this->generateOrangeLogicFieldValue($result, $source_field_definition);
    }
    else {
      $asset_source = $this->orangeLogicResultRenderer->getAssetMediaSource($result);
      $asset_filename = $this->orangeLogicResultRenderer->getAssetFileName($result);

      // Create the file:
      $asset = file_get_contents($asset_source);
      $file = file_save_data($asset, 'public://' . $asset_filename, FileSystemInterface::EXISTS_REPLACE);

      return ['target_id' => $file->id()];
    }
  }

  /**
   * Generates the required array of values by Orange Logic field type.
   *
   * The data is based on the $result variable. If the field is configured as
   * "Video" or "Audio" fill their dedicated metadata. It doesn't in other case.
   *
   * @param object $result
   *   The asset data from OL.
   * @param \Drupal\field\Entity\FieldConfig $source_field_definition
   *   The field config definition of the Orange Logic field type.
   *
   * @return array
   *   The array of values with the Asset metadata.
   */
  protected function generateOrangeLogicFieldValue(object $result, FieldConfig $source_field_definition): array {
    // Fill the common media metadata:
    $media_source_field_value = [
      'system_id' => !empty($result->SystemIdentifier) ? $result->SystemIdentifier : '',
      'title' => !empty($result->Title) ? $result->Title : '',
      'original_title' => !empty($result->ParentFolderTitle) ? $result->ParentFolderTitle : '',
      'caption' => !empty($result->Caption) ? $result->Caption : '',
      'caption_long' => !empty($result->CaptionLong) ? $result->CaptionLong : '',
      'artist' => !empty($result->Artist) ? $result->Artist : '',
      'media_type' => !empty($result->MediaType) ? $result->MediaType : '',
      'copyright' => !empty($result->copyright) ? $result->copyright : '',
      'max_width' => !empty($result->MaxWidth) ? $result->MaxWidth : '',
      'max_height' => !empty($result->MaxHeight) ? $result->MaxHeight : '',
    ];

    // Fill the media date metadata:
    if (!empty($result->MediaDate)) {
      $media_source_field_value['media_date'] = strtotime($result->MediaDate);
    }

    $media_source_media_type = $source_field_definition
      ->getSettings()['media_type'];

    if ($media_source_media_type === 'Video') {
      // In case of OrangeLogicMediaSource media_type video : We set the
      // the metadata related to videos.
      $video_url = $this->orangeLogicAssetPublicLinkManager->getPublicLink(
        $result->SystemIdentifier,
        'WebHigh',
        $result->Path_WebHigh->URI
      );

      $media_source_field_value['video_high_url'] = $video_url;
      $media_source_field_value['video_low_url'] = $video_url;

      // We define the thumbnail from the highest quality image.
      $media_source_field_value['thumbnail_url'] = $result->Path_TR1->URI;
    }
    elseif ($media_source_media_type === 'Audio') {
      // In case of OrangeLogicMediaSource media_type video : We set the
      // the metadata related to videos.
      $audio_url = $this->orangeLogicAssetPublicLinkManager->getPublicLink(
        $result->SystemIdentifier,
        'Web',
        $result->Path_Web->URI
      );

      $media_source_field_value['audio_url'] = $audio_url;
    }

    return $media_source_field_value;
  }

}
