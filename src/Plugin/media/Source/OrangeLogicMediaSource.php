<?php

namespace Drupal\media_orange_logic\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media_orange_logic\OrangeLogicResultRenderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media type plugin.
 *
 * @MediaSource(
 *   id = "orange_logic_source",
 *   label = @Translation("Orange Logic"),
 *   description = @Translation("Provides business logic and metadata for Orange Logic."),
 *   allowed_field_types = {"orange_logic_field"},
 * )
 */
class OrangeLogicMediaSource extends MediaSourceBase {

  /**
   * The directory where thumbnails are stored.
   *
   * @var string
   */
  protected $thumbsDirectory = 'public://orange_logic_thumbnails';

  /**
   * The OrangeLogicResultRenderer.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicResultRenderer
   */
  protected $olResultRenderer;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\media_orange_logic\OrangeLogicResultRenderer $ol_result_renderer
   *   The OrangeLogicResultRenderer service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, OrangeLogicResultRenderer $ol_result_renderer) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $entity_field_manager,
      $field_type_manager,
      $config_factory
    );

    $this->olResultRenderer = $ol_result_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('media_orange_logic.renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'system_id'           => $this->t('System ID'),
      'title'               => $this->t('Title'),
      'caption'             => $this->t('Caption'),
      'caption_long'        => $this->t('Caption Long'),
      'artist'              => $this->t('Artist'),
      'media_type'          => $this->t('Media Type'),
      'copyright'           => $this->t('Copyright'),
      'video_high_url'      => $this->t('Video High Url'),
      'video_low_url'       => $this->t('Video Low Url'),
      'audio_url'           => $this->t('Audio Url'),
      'thumbnail_url'       => $this->t('Thumbnail Url'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    $source_field = $this->configuration['source_field'];

    switch ($name) {
      case 'system_id':
      case 'media_type':
      case 'video_high_url':
      case 'video_low_url':
      case 'audio_url':
        if (!isset($media->{$source_field}->{$name})) {
          return parent::getMetadata($media, $name);
        }

        return $media->{$source_field}->{$name};

      case 'title':
      case 'caption':
      case 'caption_long':
      case 'artist':
        if (!isset($media->{$source_field}->{$name})) {
          return parent::getMetadata($media, $name);
        }

        // We try to get it from the an alternative field:
        $field_name = 'field_' . $name;
        if (!$media->isNew() && $media->hasField($field_name)) {
          $metadata = '';

          if (!$media->{$field_name}->isEmpty()) {
            $metadata = $media->{$field_name}->getValue()[0]['value'];
          }

          return $metadata;
        }

        return $media->{$source_field}->{$name};

      case 'copyright':
        if (!isset($media->{$source_field}->copyright)) {
          return parent::getMetadata($media, $name);
        }

        if (!$media->isNew() && $media->hasField('field_credit')) {
          return ($value = $media->field_credit->getValue()) ? (isset($value[0]['value'])) ? $value[0]['value'] : '' : '';
        }

        return $media->{$source_field}->copyright;

      case 'thumbnail_uri':
        if (!$media->get('thumbnail')->isEmpty()) {
          /** @var \Drupal\file\Entity\File $thumbnail */
          $thumbnail = $media->get('thumbnail')->referencedEntities()[0];
          return $thumbnail->getFileUri();
        }
        elseif (!empty($media->{$source_field}->thumbnail_url)) {
          $source_field = (object) $media->get($source_field)->getValue()[0];
          $thumbnail_url = $source_field->thumbnail_url;

          // Generate the thumbnails from the highest quality image.
          $asset = file_get_contents($thumbnail_url);
          $file_name = $this->olResultRenderer->getAssetFileName($source_field, $thumbnail_url);
          $destination = $this->thumbsDirectory . '/' . $file_name;
          $file = file_save_data($asset, $destination, FileSystemInterface::EXISTS_RENAME);

          return $file instanceof FileInterface ? $file->getFileUri() : parent::getMetadata($media, $name);
        }
        else {
          return parent::getMetadata($media, $name);
        }
        break;

      default:
        return parent::getMetadata($media, $name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'source_field' => 'field_media_orange_logic_source',
    ];
  }

}
