<?php

namespace Drupal\media_orange_logic\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\image\Entity\ImageStyle;

/**
 * Plugin implementation of the 'orange_logic_audio' formatter.
 *
 * @FieldFormatter(
 *   id = "orange_logic_video",
 *   label = @Translation("Orange Logic Video Formatter"),
 *   field_types = {
 *     "orange_logic_field"
 *   }
 * )
 */
class OrangeLogicVideoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'poster_image_style' => 'thumbnail',
      'poster_status' => FALSE,
      'muted' => TRUE,
      'controls' => TRUE,
      'autoplay' => FALSE,
      'loop' => FALSE,
      'width' => '',
      'height' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['muted'] = [
      '#title' => $this->t('Muted'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('muted'),
    ];

    $form['controls'] = [
      '#title' => $this->t('Show playback controls'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('controls'),
    ];

    $form['autoplay'] = [
      '#title' => $this->t('Autoplay'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('autoplay'),
    ];

    $form['loop'] = [
      '#title' => $this->t('Loop'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('loop'),
    ];

    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Video width'),
      '#default_value' => $this->getSetting('width'),
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => $this->t('pixels'),
      '#min' => 0,
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Video Height'),
      '#default_value' => $this->getSetting('height'),
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => $this->t('pixels'),
      '#min' => 0,
    ];

    $form['poster_status'] = [
      '#title' => $this->t('Display a video poster'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('poster_status'),
    ];

    $form['poster_image_style'] = [
      '#title' => $this->t('Poster Image style'),
      '#type' => 'select',
      '#options' => image_style_options(),
      '#default_value' => $this->getSetting('poster_image_style'),
      '#states' => [
        'visible' => [
          ':input[name*="poster_status"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Muted: %muted', ['%muted' => $this->getSetting('muted') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Playback controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('visible') : $this->t('hidden')]);
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('yes') : $this->t('no')]);

    if (
      !empty($this->getSetting('width')) ||
      !empty($this->getSetting('height'))
    ) {
      $summary[] = $this->t('Size: %width x %height pixels', [
        '%width' => $this->getSetting('width'),
        '%height' => $this->getSetting('height'),
      ]);
    }

    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Display Poster : %status', ['%status' => $this->getSetting('poster_status') ? $this->t('yes') : $this->t('no')]);

    if ((bool) $this->getSetting('poster_status')) {
      $summary[] = $this->t('Poster Image style: %style', ['%style' => $this->getSetting('poster_image_style')]);
    }

    return $summary;
  }

  /**
   * Initialize the attributes according to the the configuration.
   *
   * @return \Drupal\Core\Template\Attribute
   *   The Attribute object.
   */
  protected function prepareAttributes() {
    $attributes = new Attribute();

    foreach (['controls', 'autoplay', 'loop', 'muted'] as $attribute) {
      if ($this->getSetting($attribute)) {
        $attributes->setAttribute($attribute, $attribute);
      }
    }

    // Only apply if it has any value.
    $width = $this->getSetting('width');
    if (!empty($width)) {
      $attributes->setAttribute('width', $width);
    }

    // Only apply if it has any value.
    $width = $this->getSetting('height');
    if (!empty($width)) {
      $attributes->setAttribute('height', $width);
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $entity = $items->getEntity();

    foreach ($items as $delta => $item) {
      $attributes = $this->prepareAttributes();

      // We need to do this here instead of doing it at ::prepareAttributes()
      // because we need the current entity context.
      if ($this->isPosterActive()) {
        $attributes->setAttribute('poster', $this->getPoster($entity));
      }

      $elements[$delta] = [
        '#theme' => 'orange_logic_video',
        '#asset_url' => !empty($item->video_high_url) ? $item->video_high_url : '',
        '#attributes' => $attributes,
        // @TODO Calculate this using an guesser based on the url.
        '#asset_type' => 'video/mp4',
      ];
    }

    return $elements;
  }

  /**
   * Checks if the poster is configured to be displayed or not.
   *
   * @return bool
   *   TRUE is the poster should ve visible.
   */
  protected function isPosterActive() {
    return (bool) $this->getSetting('poster_status');
  }

  /**
   * Gets the poster uri based on the thumbnail image.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The media entity.
   *
   * @return string
   *   The string uri.
   */
  public function getPoster(EntityInterface $entity) {
    $poster_url = '';
    $poster_uri = '';

    if (!$entity->get('thumbnail')->isEmpty()) {
      /** @var \Drupal\file\Entity\File $poster_file */
      $poster_file = $entity->get('thumbnail')->referencedEntities()[0];
      if (!empty($poster_file)) {
        $poster_uri = $poster_file->getFileUri();
      }
    }

    if (!empty($poster_uri)) {
      // We generate the poster URL using the configured image style.
      $image_style = ImageStyle::load($this->getSetting('poster_image_style'));

      if (!empty($image_style) && $image_style->supportsUri($poster_uri)) {
        $poster_url = file_url_transform_relative($image_style->buildUrl($poster_uri));
      }
    }

    return $poster_url;
  }

}
