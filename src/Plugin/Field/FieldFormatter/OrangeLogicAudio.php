<?php

namespace Drupal\media_orange_logic\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'orange_logic_audio' formatter.
 *
 * @FieldFormatter(
 *   id = "orange_logic_audio",
 *   label = @Translation("Orange Logic Audio Formatter"),
 *   field_types = {
 *     "orange_logic_field"
 *   }
 * )
 */
class OrangeLogicAudio extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'orange_logic_audio',
        '#asset_url' => !empty($item->audio_url) ? $item->audio_url : '',
      ];
    }

    return $elements;
  }

}
