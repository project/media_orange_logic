<?php

namespace Drupal\media_orange_logic\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'orange_logic_field' field type.
 *
 * @FieldType(
 *   id = "orange_logic_field",
 *   label = @Translation("Orange Logic"),
 *   description = @Translation("Select and extract a media from Orange Logic."),
 *   module = "media_orange_logic",
 *   category = @Translation("Media"),
 *   default_widget = "orange_logic_default",
 *   default_formatter = "orange_logic_audio"
 * )
 */
class OrangeLogicField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'system_id' => [
          'type' => 'varchar',
          'length' => 20,
          'not null' => TRUE,
        ],
        'title' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'original_title' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'media_date' => [
          'type' => 'int',
          'not null' => FALSE,
        ],
        'caption' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
        'caption_long' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
        'artist' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'media_type' => [
          'type' => 'varchar',
          'length' => 20,
          'not null' => FALSE,
        ],
        'copyright' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'video_high_url' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'video_low_url' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'audio_url' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'thumbnail_url' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ],
        'max_width' => [
          'type' => 'int',
          'not null' => FALSE,
        ],
        'max_height' => [
          'type' => 'int',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('system_id')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['system_id'] = DataDefinition::create('string')->setLabel(t('System ID'));
    $properties['title'] = DataDefinition::create('string')->setLabel(t('Title'));
    $properties['original_title'] = DataDefinition::create('string')->setLabel(t('Original Submission Title'));
    $properties['media_date'] = DataDefinition::create('timestamp')->setLabel(t('Media Date'));
    $properties['caption'] = DataDefinition::create('string')->setLabel(t('Caption'));
    $properties['caption_long'] = DataDefinition::create('string')->setLabel(t('Caption Long'));
    $properties['artist'] = DataDefinition::create('string')->setLabel(t('Artist'));
    $properties['copyright'] = DataDefinition::create('string')->setLabel(t('Copyright'));
    $properties['media_type'] = DataDefinition::create('string')->setLabel(t('Media Type'));
    $properties['video_high_url'] = DataDefinition::create('string')->setLabel(t('Web High Video'));
    $properties['video_low_url'] = DataDefinition::create('string')->setLabel(t('Web Low Video'));
    $properties['audio_url'] = DataDefinition::create('string')->setLabel(t('Web Audio'));
    $properties['thumbnail_url'] = DataDefinition::create('string')->setLabel(t('Thumbnail url'));
    $properties['max_width'] = DataDefinition::create('integer')->setLabel(t('Max width'));
    $properties['max_height'] = DataDefinition::create('integer')->setLabel(t('Max Height'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['media_type'] = [
      '#title' => $this->t('Media Type'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('media_type'),
      '#options' => [
        'Image' => 'Image',
        'Video' => 'Video',
        'Audio' => 'Audio',
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'media_type' => 'Image',
    ];
  }

}
