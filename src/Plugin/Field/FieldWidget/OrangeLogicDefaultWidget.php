<?php

namespace Drupal\media_orange_logic\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A widget to input the Orange Logic Data.
 *
 * @FieldWidget(
 *   id = "orange_logic_default",
 *   module = "media_orange_logic",
 *   label = @Translation("Orange Logic Selector"),
 *   field_types = {
 *     "orange_logic_field"
 *   }
 * )
 */
class OrangeLogicDefaultWidget extends WidgetBase {

  /**
   * The DateFormatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The DateFormatter service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, DateFormatter $date_formatter) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];

    $element += [
      '#type' => 'fieldset',
    ];

    if (!empty($item->media_date)) {
      $formatted_date = $this->dateFormatter->format($item->media_date, 'medium');
      $element['media_date'] = [
        '#markup' => $this->t(
          'Media date: @media_date',
          ['@media_date' => $formatted_date]
        ),
      ];
    }

    $element['system_id'] = [
      '#title' => $this->t('System Identifier (URN)'),
      '#type' => 'textfield',
      '#default_value' => isset($item->system_id) ? $item->system_id : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['title'] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#default_value' => isset($item->title) ? $item->title : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['original_title'] = [
      '#title' => $this->t('Original submission Title'),
      '#type' => 'textfield',
      '#default_value' => isset($item->original_title) ? $item->original_title : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['caption'] = [
      '#title' => $this->t('Caption'),
      '#type' => 'textarea',
      '#default_value' => isset($item->caption) ? $item->caption : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['caption_long'] = [
      '#title' => $this->t('Caption long'),
      '#type' => 'textarea',
      '#default_value' => isset($item->caption_long) ? $item->caption_long : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['artist'] = [
      '#title' => $this->t('Artist'),
      '#type' => 'textfield',
      '#default_value' => isset($item->artist) ? $item->artist : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['copyright'] = [
      '#title' => $this->t('Copyright'),
      '#type' => 'textfield',
      '#default_value' => isset($item->copyright) ? $item->copyright : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['media_type'] = [
      '#title' => $this->t('Media Type'),
      '#type' => 'textfield',
      '#default_value' => isset($item->media_type) ? $item->media_type : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['max_width'] = [
      '#title' => $this->t('Max width'),
      '#type' => 'number',
      '#default_value' => isset($item->max_width) ? $item->max_width : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    $element['max_height'] = [
      '#title' => $this->t('Max height'),
      '#type' => 'number',
      '#default_value' => isset($item->max_height) ? $item->max_height : '',
      '#attributes' => ['disabled' => TRUE],
    ];

    // Dedicated fields in case of : Video media type.
    if ($this->isVideoMediaType($items)) {
      $element['video_high_url'] = [
        '#title' => $this->t('Video High URL'),
        '#type' => 'textfield',
        '#default_value' => isset($item->video_high_url) ? $item->video_high_url : '',
        '#attributes' => ['disabled' => TRUE],
      ];

      $element['video_low_url'] = [
        '#title' => $this->t('Video low URL'),
        '#type' => 'textfield',
        '#default_value' => isset($item->video_high_url) ? $item->video_high_url : '',
        '#attributes' => ['disabled' => TRUE],
      ];
    }

    // Dedicated fields in case of : Video media type.
    if ($this->isAudioMediaType($items)) {
      $element['audio_url'] = [
        '#title' => $this->t('Original audio URL'),
        '#type' => 'textfield',
        '#default_value' => isset($item->audio_url) ? $item->audio_url : '',
        '#attributes' => ['disabled' => TRUE],
      ];
    }

    return $element;
  }

  /**
   * Checks if the field instance is configured as media_type Video.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The FieldItemListInterface.
   *
   * @return bool
   *   TRUE if the configured media_type at the field instance is Video.
   */
  protected function isVideoMediaType(FieldItemListInterface $items) {
    return $items->getSettings()['media_type'] === 'Video';
  }

  /**
   * Checks if the field instance is configured as media_type Audio.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The FieldItemListInterface.
   *
   * @return bool
   *   TRUE if the configured media_type at the field instance is Audio.
   */
  protected function isAudioMediaType(FieldItemListInterface $items) {
    return $items->getSettings()['media_type'] === 'Audio';
  }

}
