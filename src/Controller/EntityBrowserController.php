<?php

namespace Drupal\media_orange_logic\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\media_orange_logic\OrangeLogicManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EntityBrowserController.
 */
class EntityBrowserController extends ControllerBase {

  /**
   * The OrangeLogicManagerInterface service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicManagerInterface
   */
  protected $orangeLogicManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a BookController object.
   *
   * @param \Drupal\media_orange_logic\OrangeLogicManagerInterface $media_orange_logic_manager
   *   The OrangeLogicManagerInterface service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(OrangeLogicManagerInterface $media_orange_logic_manager, RendererInterface $renderer) {
    $this->orangeLogicManager = $media_orange_logic_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_orange_logic.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Ajax controller to return the thumbnails of the given list of assets ids.
   *
   * @throws \Exception
   */
  public function ajaxSelectedAssets(Request $request) {
    $query = [
      'SystemIdentifier' => $request->request->all('asset_ids'),
    ];

    $response = $this->orangeLogicManager->search(
      $this->orangeLogicManager->buildCriteriaQuery($query)
    );

    $response_rendered = $response->renderItems();

    return new JsonResponse(
      $this->renderer->render($response_rendered)
    );
  }

}
