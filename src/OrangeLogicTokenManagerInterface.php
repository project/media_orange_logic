<?php

namespace Drupal\media_orange_logic;

/**
 * Interface OrangeLogicTokenManagerInterface.
 */
interface OrangeLogicTokenManagerInterface {

  /**
   * Retrieves a valid token.
   *
   * If the current stored token is not valid. It requests a new one to the
   * service.
   *
   * @return string
   *   A valid token string.
   */
  public function getToken() : string;

}
