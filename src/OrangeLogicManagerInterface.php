<?php

namespace Drupal\media_orange_logic;

/**
 * Interface OrangeLogicManagerInterface.
 */
interface OrangeLogicManagerInterface {

  /**
   * Get medias according to the given filters.
   *
   * @param string $query
   *   The query string.
   * @param array $options
   *   The query options.
   *
   * @return OrangeLogicSearchResponse
   */
  public function search(string $query, array $options = []) : OrangeLogicSearchResponse;

}
