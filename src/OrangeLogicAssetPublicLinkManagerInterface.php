<?php

namespace Drupal\media_orange_logic;

/**
 * Interface OrangeLogicAssetPublicLinkManagerInterface.
 */
interface OrangeLogicAssetPublicLinkManagerInterface {

  /**
   * Retrieves the Asset public link from the Orange Logic service.
   *
   * @param string $systemId
   *   The asset SystemIdentifier.
   * @param string $format
   *   The format.
   *   It can be 'WebHigh' or 'Web'.
   * @param string $default_url
   *   The default url to be used as default case.
   * @param int $download
   *   The download param.
   *
   * @return string
   *   The public url of the requested asset.
   */
  public function getPublicLink(string $systemId, string $format, string $default_url, int $download = 0) : string;

}
