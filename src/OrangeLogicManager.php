<?php

namespace Drupal\media_orange_logic;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class OrangeLogicManager.
 *
 * Handles the interaction with Orange Logic.
 */
class OrangeLogicManager implements OrangeLogicManagerInterface {

  /**
   * The format on which we want to receive the responses.
   */
  const ORANGE_LOGIC_SEARCH_FORMAT = 'json';

  /**
   * The default list of fields to be retrieved from the search service.
   */
  const DEFAULT_FIELDS = [
    'SystemIdentifier', 'Path_TR6', 'Path_TR1', 'Path_TR3',
    'Title', 'ParentFolderTitle', 'Caption', 'CaptionLong', 'copyright',
    'MediaDate', 'MediaType', 'DocSubType', 'Artist', 'Path_WebHigh',
    'Path_WebLow', 'Path_Web', 'Link', 'Country', 'MaxWidth', 'MaxHeight',
    'Path_CMS1'
  ];

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The OrangeLogicTokenManagerInterface service.
   *
   * @var \Drupal\media_orange_logic\OrangeLogicTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The ConfigFactory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new OrangeLogicManager object.
   */
  public function __construct(ClientInterface $http_client, OrangeLogicTokenManagerInterface $token_manager, ConfigFactoryInterface $config_factory) {
    $this->httpClient = $http_client;
    $this->tokenManager = $token_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public function search(string $query, array $options = []) : OrangeLogicSearchResponse {
    $config = $this->configFactory->get('media_orange_logic.settings');
    $search_endpoint = $config->get('search_endpoint');

    if (empty($search_endpoint)) {
      // ToDo: check if config is set properly.
    }

    $query_data = [
      'query' => [
        'query' => $query,
        'token' => $this->tokenManager->getToken(),
        'format' => self::ORANGE_LOGIC_SEARCH_FORMAT,
        'fields' => implode(',', self::getDefaultFields()),
      ],
    ];

    if (!empty($options['page_number'])) {
      $query_data['query']['pagenumber'] = $options['page_number'];
    }

    if (!empty($options['per_page'])) {
      $query_data['query']['countperpage'] = $options['per_page'];
    }

    try {
      $response = $this->httpClient->request(
        'GET',
        $search_endpoint,
        $query_data
      );
    }
    catch (GuzzleException $e) {
      die ($e->getMessage());
    }

    if (!empty($response)) {
      $data = $response->getBody()->getContents();
      $search_response = new OrangeLogicSearchResponse(
        $data
      );

      return $search_response;
    }
    return new OrangeLogicSearchResponse('[]');
  }

  /**
   * Builds a query string based on the given $values.
   *
   * @param array $values
   *   The array of key:value criteria.
   *
   * @return string
   *   The query string based on the given filters.
   */
  public function buildCriteriaQuery(array $values) {
    $query_params = [];
    // Filter the form values to get only the allowed ones:
    $this->filterAllowedCriteriaFilters($values);

    foreach ($values as $key => $value) {
      if (!empty($value)) {
        $query_params[] = $this->buildCriteriaQueryParam($key, $value);
      }
    }

    // Filter empty values:
    $query_params = array_filter($query_params);
    return implode(' AND ', $query_params);
  }

  /**
   * Formats a filter Criteria into the correct query format.
   *
   * @param string $key
   *   The criteria key.
   * @param mixed $value
   *   The criteria value.
   *
   * @return string
   *   The formatted query string.
   */
  protected function buildCriteriaQueryParam(string $key, $value) {
    $params = [];

    if (is_array($value)) {
      foreach ($value as $item) {
        // We don't add empty parameters:
        if (!empty($value) && $item !== 0 && $item !== 'all') {
          $params[] = $key . ':' . $item;
        }
      }
    }
    else {
      if (!empty($value) && $value !== 0 && $value !== 'all') {
        $params[] = $key . ':' . $value;
      }
    }
    return empty($params) ? '' : '(' . implode(' OR ', $params) . ')';
  }

  /**
   * Filters the given $values to have only those values from allowed keys.
   *
   * @param array $values
   *   The filtered array of values.
   */
  protected function filterAllowedCriteriaFilters(array &$values) : void {
    $values = array_intersect_key(
      $values,
      array_flip(self::getAllowedCriteriaFilters())
    );
  }

  /**
   * Retrieves the array of allowed filter Criteria by the API.
   *
   * @return array
   *   The array of allowed criteria keys by the API.
   */
  protected static function getAllowedCriteriaFilters() : array {
    return [
      'MediaType', 'Text', 'Artist', 'Keyword', 'NativeKeyword',
      'SystemIdentifier', 'MediaNumber', 'Color', 'Orientation',
      'OriginalSubmisionNumber', 'CreateDate', 'EditDate', 'MediaDate',
    ];
  }

  /**
   * Retrieves the default list of fields to be retrieved from the service.
   *
   * @return array
   *   The default list of fields to be retrieved from the search service.
   */
  protected static function getDefaultFields() : array {
    return static::DEFAULT_FIELDS;
  }

}
